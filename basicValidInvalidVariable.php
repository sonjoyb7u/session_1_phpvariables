      <!-- PHP.NET Menually -->
<?php

  // <!--  BASIC  -->

  $var = 'Bob';
  $Var = 'Joe';
  echo "$var, $Var";         //Outputs

  echo "<br/>";

// To assign by reference, simply prepend an ampersand (&) to the beginning of the variable which is being assigned

  $foo = 'Bob';              // Assign the value 'Bob' to $foo
  $bar = &$foo;              // Reference $foo via $bar.
  $bar = "My name is $bar";  // Alter $bar...
  echo $bar;
  echo "<br/>";
  echo $foo;                 // $foo is altered too.


  echo "<br/>";



  $foo = 25;
  $bar = &$foo;      // This is a valid assignment.
  //$bar = &$foo(24 * 7);  // Invalid; references an unnamed expression.

  function test($bar)
  {
     return $bar;
  }

  //$bar = &test();    // Invalid.
  // echo test(30);

  $obj = test(30);
  echo $obj;

  echo "<br/>";


  // Boolean usage; outputs 'false' (See ternary operators for more on this syntax)
  echo($unset_bool ? "true\n" : "false\n");

  echo "<br/>";
  // String usage; outputs 'string(3) "abc"'
  $unset_str .= 'abc';
  var_dump($unset_str);

  echo "<br/>";
  // Integer usage; outputs 'int(25)'
  $unset_int += 25; // 0 + 25 => 25
  var_dump($unset_int);

  echo "<br/>";
?>
